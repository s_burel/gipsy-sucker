/**
 * Format Leagues
 */
function formatLeague (league) {
  if (league.includes("Ligue 1"))
    league = "Ligue 1";
  if (league.includes("Premier League"))
    league = "Premier League";
  if (league.includes("Champions League"))
    league = "Champions League";
  if (league.includes("Europa League"))
    league = "Europa League";
  if (league.includes("UEFA Cup"))
    league = "Europa League";
  if (league.includes("Euro U21"))
    league = "Euro U21";
  if (league.includes("Bundesliga"))
    league = "Bundesliga";
  if (league.includes("Serie A"))
    league = "Serie A";
  if (league.includes("Primera Division"))
    league = "LaLiga";
  if (league.includes("LaLiga"))
    league = "LaLiga";
  return league;
}

/**
 * Save Data from params to localstorage
 */
function saveDataToLocalStorage(data) {
    var a = [];
    // Parse the serialized data back into an aray of objects
    if (isDefined(localStorage["gipsy"]))
      a = JSON.parse(localStorage.getItem('gipsy'));
    // Push the new data (whether it be an object or anything else) onto the array
    a.push(data);
    // Re-serialize the array back into a string and store it in localStorage
    localStorage.setItem('gipsy', JSON.stringify(a));
}


/**
 * Sleep
 */
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}


/**
 * Check if a member is
 */
function isDefined(x) {
  var undefined;
  return x !== undefined;
}


/**
 * Format date string from string Month to int.toString() month
 * params : A string including the month written in 3 letters alphabet format
 */
function formatDate(date) {
  date = date.replace("Jan","01");
  date = date.replace("Feb","02");
  date = date.replace("Mar","03");
  date = date.replace("Apr","04");
  date = date.replace("May","05");
  date = date.replace("Jun","06");
  date = date.replace("Jul","07");
  date = date.replace("Aug","08");
  date = date.replace("Sep","09");
  date = date.replace("Oct","10");
  date = date.replace("Nov","11");
  date = date.replace("Dec","12");
  var result = date.split(' ');
  return result;
}


/**
 * Fetch all usefull informations on result table
 * params : DOM Node of the table result from a oddsportal sport league
 */
function fetch(node) {
  // Instantiation
  var play = new Object();

  // Insert league
  var league = node.lastChild.firstChild.firstChild.lastChild.text;
  league = formatLeague(league);
  play.league = league;

  node = node.lastChild.childNodes;
  console.log(node);

  // For Each node of the table result :
  node.forEach((element) => {
    // If node contains a date
    if(element.className === "center nob-border")
      play.date =
        formatDate(element.childNodes[0].childNodes[0].textContent);
    // If node contains a play
    if(element.className != "dark center"
    && element.className != "center nob-border"
    && element.className != "table-dummyrow"
    && element.className != "odd hidden"
    && element.className != "hidden") {
      play.teams =
        element.childNodes[1].lastChild.textContent.split(" - ");
      play.score = "9:9";
      play.cote = [
        element.childNodes[2].textContent,
        element.childNodes[3].textContent,
        element.childNodes[4].textContent
        ];
      play.bookies = element.lastChild.textContent;
      // Save result
      saveDataToLocalStorage(play);
    }
  });
}


/**
 * Get next url of current league
 * params : DOM Node of the main content from a oddsportal sport league
 */
function nextUrl(node) {
  // Get usefull information
  var pagination = node.childNodes[10].childNodes[1].childNodes;
  var current = pagination.length;
  var next = pagination[current-2].href;
  // If we must change the year of the league
  if (next === window.location.href) {
    var childrens = node.childNodes[7].childNodes[0].childNodes;
      // For Each year of the years list :
    childrens.forEach((element) => {
      // Search for the active element
      if(element.firstChild.className === "active")
        if(element.nextSibling)
          next = element.nextSibling.firstChild.firstChild.firstChild.href;
        else next = null;
    });
  }
  console.log(next);
  return next;
}


/**
 * Run Function : Fetch 
 */
function run() {
  try {
    // Instantiation
    var main = document.getElementById("col-content");
    var node = document.getElementById("tournamentTable");
    var table = node.childNodes[0].childNodes[1].childNodes;

    fetch(node);
    // var next = nextUrl(main);

    // sleep(1000).then(() => {
        // window.open(next, '_blank');
        // window.close();
      // Do something after the sleep!
      // })
  } catch (e) {
   // statements to handle any exceptions
   console.log(e); // pass exception object to error handler
  }
}

window.addEventListener ("load", run, false);
